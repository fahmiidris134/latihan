<?php /* Smarty version Smarty-3.1.19, created on 2017-04-18 11:39:06
         compiled from "module:bankbni/views/templates/hook/_partials/payment_infos.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1392058f5986a432489-01076090%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'aae247eb68b4bfd21af9d246944d5e1d56d0919d' => 
    array (
      0 => 'module:bankbni/views/templates/hook/_partials/payment_infos.tpl',
      1 => 1492489898,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '1392058f5986a432489-01076090',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'total' => 0,
    'bankbniOwner' => 0,
    'bankbniDetails' => 0,
    'bankbniAddress' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58f5986a45d409_75973239',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58f5986a45d409_75973239')) {function content_58f5986a45d409_75973239($_smarty_tpl) {?>


<dl>
    <dt><?php echo smartyTranslate(array('s'=>'Amount','mod'=>'bankbni'),$_smarty_tpl);?>
</dt>
    <dd><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['total']->value, ENT_QUOTES, 'UTF-8');?>
</dd>
    <dt><?php echo smartyTranslate(array('s'=>'Name of account owner','mod'=>'bankbni'),$_smarty_tpl);?>
</dt>
    <dd><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['bankbniOwner']->value, ENT_QUOTES, 'UTF-8');?>
</dd>
    <dt><?php echo smartyTranslate(array('s'=>'Please include these details','mod'=>'bankbni'),$_smarty_tpl);?>
</dt>
    <dd><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['bankbniDetails']->value, ENT_QUOTES, 'UTF-8');?>
</dd>
    <dt><?php echo smartyTranslate(array('s'=>'Bank name','mod'=>'bankbni'),$_smarty_tpl);?>
</dt>
    <dd><?php echo $_smarty_tpl->tpl_vars['bankbniAddress']->value;?>
</dd>
</dl>
<?php }} ?>
