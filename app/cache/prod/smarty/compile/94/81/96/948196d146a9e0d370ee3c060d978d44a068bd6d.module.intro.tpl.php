<?php /* Smarty version Smarty-3.1.19, created on 2017-04-18 11:39:06
         compiled from "module:bankbni/views/templates/hook/intro.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1494758f5986a3d4861-06356756%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '948196d146a9e0d370ee3c060d978d44a068bd6d' => 
    array (
      0 => 'module:bankbni/views/templates/hook/intro.tpl',
      1 => 1492489898,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '1494758f5986a3d4861-06356756',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'bankwireReservationDays' => 0,
    'bankbniCustomText' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58f5986a40f1f0_39964591',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58f5986a40f1f0_39964591')) {function content_58f5986a40f1f0_39964591($_smarty_tpl) {?>

<section>
  <p>
    <?php echo smartyTranslate(array('s'=>'Please transfer the invoice amount to our bank account. You will receive our order confirmation per email containing bank details and order number.','mod'=>'ps_wirepaymentbankbni'),$_smarty_tpl);?>

    <?php echo smartyTranslate(array('s'=>'Goods will be reserved %s days for you and we\'ll process the order immediately after receiving the payment.','sprintf'=>array($_smarty_tpl->tpl_vars['bankwireReservationDays']->value),'mod'=>'bankbni'),$_smarty_tpl);?>

    <?php if ($_smarty_tpl->tpl_vars['bankbniCustomText']->value) {?>
        <a data-toggle="modal" data-target="#bankbni-modal"><?php echo smartyTranslate(array('s'=>'More information','mod'=>'bankbni'),$_smarty_tpl);?>
</a>
    <?php }?>
  </p>

  <div class="modal fade" id="bankbni-modal" tabindex="-1" role="dialog" aria-labelledby="Bank BNI information" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h2><?php echo smartyTranslate(array('s'=>'Bank BNI','mod'=>'bankbni'),$_smarty_tpl);?>
</h2>
        </div>
        <div class="modal-body">
          <p><?php echo smartyTranslate(array('s'=>'Payment is made by transfer of the invoice amount to the following account:','mod'=>'bankbni'),$_smarty_tpl);?>
</p>
          <?php echo $_smarty_tpl->getSubTemplate ('module:bankbni/views/templates/hook/_partials/payment_infos.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

          <?php echo $_smarty_tpl->tpl_vars['bankbniCustomText']->value;?>

        </div>
      </div>
    </div>
  </div>
</section>
<?php }} ?>
