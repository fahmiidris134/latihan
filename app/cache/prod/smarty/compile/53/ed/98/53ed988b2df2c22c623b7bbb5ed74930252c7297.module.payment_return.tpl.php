<?php /* Smarty version Smarty-3.1.19, created on 2017-04-18 11:48:14
         compiled from "module:bankbni/views/templates/hook/payment_return.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1946558f59a8ebc1545-11682434%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '53ed988b2df2c22c623b7bbb5ed74930252c7297' => 
    array (
      0 => 'module:bankbni/views/templates/hook/payment_return.tpl',
      1 => 1492489898,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '1946558f59a8ebc1545-11682434',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'status' => 0,
    'shop_name' => 0,
    'reference' => 0,
    'contact_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58f59a8ed24d11_02363527',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58f59a8ed24d11_02363527')) {function content_58f59a8ed24d11_02363527($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['status']->value=='ok') {?>
    <p>
      <?php echo smartyTranslate(array('s'=>'Your order on %s is complete.','sprintf'=>array($_smarty_tpl->tpl_vars['shop_name']->value),'mod'=>'bankbni'),$_smarty_tpl);?>
<br/>
      <?php echo smartyTranslate(array('s'=>'Please send us payment via Bank BNI with:','mod'=>'bankbni'),$_smarty_tpl);?>

    </p>
    <?php echo $_smarty_tpl->getSubTemplate ('module:bankbni/views/templates/hook/_partials/payment_infos.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    <p>
      <?php echo smartyTranslate(array('s'=>'Please specify your order reference %s in the bankwire description.','sprintf'=>array($_smarty_tpl->tpl_vars['reference']->value),'mod'=>'bankbni'),$_smarty_tpl);?>
<br/>
      <?php echo smartyTranslate(array('s'=>'We\'ve also sent you this information by e-mail.','mod'=>'bankbni'),$_smarty_tpl);?>

    </p>
    <strong><?php echo smartyTranslate(array('s'=>'Your order will be sent as soon as we receive payment.','mod'=>'bankbni'),$_smarty_tpl);?>
</strong>
    <p>
      <?php echo smartyTranslate(array('s'=>'If you have questions, comments or concerns, please contact our [1]expert customer support team[/1].','mod'=>'bankbni','tags'=>array("<a href='".((string)$_smarty_tpl->tpl_vars['contact_url']->value)."'>")),$_smarty_tpl);?>

    </p>
<?php } else { ?>
    <p class="warning">
      <?php echo smartyTranslate(array('s'=>'We noticed a problem with your order. If you think this is an error, feel free to contact our [1]expert customer support team[/1].','mod'=>'bankbni','tags'=>array("<a href='".((string)$_smarty_tpl->tpl_vars['contact_url']->value)."'>")),$_smarty_tpl);?>

    </p>
<?php }?>
<?php }} ?>
