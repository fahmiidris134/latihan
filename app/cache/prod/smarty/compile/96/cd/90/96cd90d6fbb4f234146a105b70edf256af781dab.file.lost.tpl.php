<?php /* Smarty version Smarty-3.1.19, created on 2017-04-18 11:16:58
         compiled from "C:\xampp\htdocs\baju\modules\welcome\views\templates\lost.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3152058f5933adb42e0-73900440%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '96cd90d6fbb4f234146a105b70edf256af781dab' => 
    array (
      0 => 'C:\\xampp\\htdocs\\baju\\modules\\welcome\\views\\templates\\lost.tpl',
      1 => 1490873256,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3152058f5933adb42e0-73900440',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58f5933adcb9e9_72077460',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58f5933adcb9e9_72077460')) {function content_58f5933adcb9e9_72077460($_smarty_tpl) {?>

<div class="onboarding onboarding-popup bootstrap">
  <div class="content">
    <p><?php echo smartyTranslate(array('s'=>'Hey! Are you lost?','d'=>'Modules.Welcome.Admin'),$_smarty_tpl);?>
</p>
    <p><?php echo smartyTranslate(array('s'=>'To continue, click here:','d'=>'Modules.Welcome.Admin'),$_smarty_tpl);?>
</p>
    <div class="text-center">
      <button class="btn btn-primary onboarding-button-goto-current"><?php echo smartyTranslate(array('s'=>'Continue','d'=>'Modules.Welcome.Admin'),$_smarty_tpl);?>
</button>
    </div>
    <p><?php echo smartyTranslate(array('s'=>'If you want to stop the tutorial for good, click here:','d'=>'Modules.Welcome.Admin'),$_smarty_tpl);?>
</p>
    <div class="text-center">
      <button class="btn btn-alert onboarding-button-stop"><?php echo smartyTranslate(array('s'=>'Quit the Welcome tutorial','d'=>'Modules.Welcome.Admin'),$_smarty_tpl);?>
</button>
    </div>
  </div>
</div>
<?php }} ?>
