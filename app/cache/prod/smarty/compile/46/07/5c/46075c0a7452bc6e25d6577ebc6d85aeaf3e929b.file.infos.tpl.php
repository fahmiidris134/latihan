<?php /* Smarty version Smarty-3.1.19, created on 2017-04-18 11:31:44
         compiled from "C:\xampp\htdocs\baju\modules\bankbni\views\templates\hook\infos.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2670758f596b05fe257-34991246%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '46075c0a7452bc6e25d6577ebc6d85aeaf3e929b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\baju\\modules\\bankbni\\views\\templates\\hook\\infos.tpl',
      1 => 1492489898,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2670758f596b05fe257-34991246',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58f596b060dc65_35385331',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58f596b060dc65_35385331')) {function content_58f596b060dc65_35385331($_smarty_tpl) {?>

<div class="alert alert-info">
<img src="../modules/bankbni/logo.png" style="float:left; margin-right:15px;" height="60">
<p><strong><?php echo smartyTranslate(array('s'=>"This module allows you to accept secure payments by Bank BNI.",'d'=>'Modules.BankBNI.Admin'),$_smarty_tpl);?>
</strong></p>
<p><?php echo smartyTranslate(array('s'=>"If the client chooses to pay by Bank BNI, the order's status will change to 'Awaiting Bank BNI Payment.'",'d'=>'Modules.BankBNI.Admin'),$_smarty_tpl);?>
</p>
<p><?php echo smartyTranslate(array('s'=>"That said, you must manually confirm the order upon receiving the Bank BNI.",'d'=>'Modules.BankBNI.Admin'),$_smarty_tpl);?>
</p>
</div>
<?php }} ?>
